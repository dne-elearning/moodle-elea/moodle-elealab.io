# Liste des Plateformes ÉLÉA

## Auvergne-Rhône-Alpes
https://aura-01.elea.apps.education.fr  
https://aura-03.elea.apps.education.fr  
https://aura-07.elea.apps.education.fr  
https://aura-15-43.elea.apps.education.fr  
https://aura-26.elea.apps.education.fr  
https://aura-38-nord.elea.apps.education.fr  
https://aura-38-sud.elea.apps.education.fr  
https://aura-42.elea.apps.education.fr  
https://aura-63.elea.apps.education.fr  
https://aura-69.elea.apps.education.fr  
https://aura-69-metro1.elea.apps.education.fr  
https://aura-69-metro2.elea.apps.education.fr  
https://aura-73.elea.apps.education.fr  
https://aura-74.elea.apps.education.fr  

## Bretagne
https://bzh-22-est.elea.apps.education.fr  
https://bzh-22-ouest.elea.apps.education.fr  
https://bzh-29-nord.elea.apps.education.fr  
https://bzh-29-sud.elea.apps.education.fr  
https://bzh-35-nord-est.elea.apps.education.fr  
https://bzh-35-rennes.elea.apps.education.fr  
https://bzh-35-sud-ouest.elea.apps.education.fr  
https://bzh-56-est.elea.apps.education.fr  
https://bzh-56-ouest.elea.apps.education.fr  

## Île-de-France
https://antony.elea.ac-versailles.fr  
https://argenteuil.elea.ac-versailles.fr  
https://boulogne.elea.ac-versailles.fr  
https://cergy.elea.ac-versailles.fr  
https://communaute.elea.ac-versailles.fr  
https://enghien.elea.ac-versailles.fr  
https://etampes.elea.ac-versailles.fr  
https://evry.elea.ac-versailles.fr  
https://gennevilliers.elea.ac-versailles.fr  
https://gonesse.elea.ac-versailles.fr  
https://mantes.elea.ac-versailles.fr  
https://massy.elea.ac-versailles.fr  
https://montgeron.elea.ac-versailles.fr  
https://mureaux.elea.ac-versailles.fr  
https://nanterre.elea.ac-versailles.fr  
https://neuilly.elea.ac-versailles.fr  
https://poissy.elea.ac-versailles.fr  
https://pontoise.elea.ac-versailles.fr  
https://rambouillet.elea.ac-versailles.fr  
https://sarcelles.elea.ac-versailles.fr  
https://savigny.elea.ac-versailles.fr  
https://sgl.elea.ac-versailles.fr  
https://sqy.elea.ac-versailles.fr  
https://vanves.elea.ac-versailles.fr  
https://versailles.elea.ac-versailles.fr  
https://idf-77-1.elea.apps.education.fr  
https://idf-77-2.elea.apps.education.fr  
https://idf-77-3.elea.apps.education.fr  
https://idf-77-4.elea.apps.education.fr  
https://idf-93-1.elea.apps.education.fr  
https://idf-93-2.elea.apps.education.fr  
https://idf-93-3.elea.apps.education.fr  
https://idf-93-4.elea.apps.education.fr  
https://idf-94-1.elea.apps.education.fr  
https://idf-94-2.elea.apps.education.fr  
https://idf-94-3.elea.apps.education.fr  

## Hauts-de-France
https://hdf-02.elea.apps.education.fr  
https://hdf-59-1-3.elea.apps.education.fr  
https://hdf-59-4-5.elea.apps.education.fr  
https://hdf-59-6-7.elea.apps.education.fr  
https://hdf-59-8-9.elea.apps.education.fr  
https://hdf-60-centrale.elea.apps.education.fr  
https://hdf-60-occidentale.elea.apps.education.fr  
https://hdf-60-orientale.elea.apps.education.fr  
https://hdf-62-10-11.elea.apps.education.fr  
https://hdf-62-12.elea.apps.education.fr  
https://hdf-62-13-14.elea.apps.education.fr  
https://hdf-80.elea.apps.education.fr  

## Outre-mer
https://guadeloupe.elea.apps.education.fr  

## Pays de la Loire
https://pdl-44-nord.elea.apps.education.fr  
https://pdl-44-pr.elea.apps.education.fr  
https://pdl-44-sud.elea.apps.education.fr  
https://pdl-49.elea.apps.education.fr  
https://pdl-49-pr.elea.apps.education.fr  
https://pdl-53.elea.apps.education.fr  
https://pdl-53-pr.elea.apps.education.fr  
https://pdl-72.elea.apps.education.fr  
https://pdl-72-pr.elea.apps.education.fr  
https://pdl-85.elea.apps.education.fr  
https://pdl-85-pr.elea.apps.education.fr  
https://pdl-ensagri.elea.apps.education.fr  

## Divers
https://feebat.elea.apps.education.fr  