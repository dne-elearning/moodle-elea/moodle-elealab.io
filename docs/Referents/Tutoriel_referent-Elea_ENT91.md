---
tags:
  - importation
  - ENT 91
  - Essonnes
  - Mon Collège
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

# Créer les comptes utilisateurs avec l'ENT Mon Collège

:::warning 
Avec le **nouveau connecteur** (multicas) mis en place à la rentrée 2024, **l'import des comptes n'est plus nécessaire**. En effet, les comptes et les cohortes sont synchronisées lors de la connexion des utilisateurs. Toutes les classes sont pleuplées au fur et à mesure de la connexion des élèves.

Cependant, si les professeurs sont inquiets de ne pas voir l'ensemble de leur élèves, il est possible de peupler toutes des classes au début de l'année scolaire en effectuant un import comme les années précédentes. 
:::

La procédure reste inchangée :

## Etape 1 - Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à https://moncollege-ent.essonne.fr avec vos identifiants puis - dans la liste des applications - choisissez **Administration** (seuls les administrateurs voient cette icône).

![administration](img/ent91/capture1.png) puis ![administration](img/ent91/admin-icon.png)

La console d'administration apparaît à l'écran :

![exports](img/ent91/console.png#printscreen#centrer)

Cliquez sur **Exporter des comptes** dans la section **Imports / Exports**.

Dans le nouvel écran qui s'affiche :

![export](img/ent91/export.png#printscreen#centrer)

1. Rendez-vous dans l'onglet **Exporter des comptes** ;
2. Vérifiez que les valeurs reprennent bien celles de la capture ci-dessus ;
3. Cliquez sur **Exporter**.

Il ne vous reste plus qu’à enregistrer le fichier **« export.csv »** à l’endroit que vous souhaitez.

![docs_à_télécharger](img/ent91/capture4.png#printscreen#centrer)

:::danger 
**Très important** : Le fichier doit impérativement se nommer  **« export.csv »** et vous devrez le détruire à la fin de la procédure ! 

Si le fichier se nomme autrement (par exemple **« export (1).csv »**), c'est que vous avez sans doute omis de supprimer le précédent export réalisé sur votre machine. Supprimez alors le plus vieux et renommez le plus récent en supprimant les parenthèses et le chiffre à l'intérieur.
:::

## Etape 2 - Se connecter à la plateforme Elea de l'établissement

Un courriel automatique est adressé au chef d'établissement via l'adresse **UAI@ac-versailles.fr**.
Il contient le lien permettant **d'activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs.**

Entrez l'url correspondant à votre plateforme de bassin sur le modèle **BASSIN.elea.ac-versailles.fr**

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![accueil_elea](img/accueil_elea/capture2_91.png#printscreen#centrer)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](img/accueil_elea/capture3.png#printscreen#centrer)

## Etape 3 - Importer vos comptes utilisateurs dans Elea

Sur la page de gestion de votre établissement, l'onglet "Accueil" vous donne l'historique de vos derniers imports.

![import](img/import/import_accueil.png#printscreen#centrer)

Cliquez sur l'onglet **« Importer des utilisateurs »**.

Puis cliquez sur **« Sélectionnez un fichier sur votre ordinateur »** ou **glissez/déposez** celui-ci.

![import](img/import/import_etape1.png#printscreen#centrer)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![création cohortes](img/import/import_etape2.png#printscreen#centrer)

![import utilisateurs](img/import/import_etape3.png#printscreen#centrer)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](img/import/import_etape4.png#printscreen#centrer)
