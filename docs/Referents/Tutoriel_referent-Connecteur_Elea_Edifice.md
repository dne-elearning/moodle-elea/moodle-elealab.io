import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

### Créer un connecteur depuis un ENT Edifice

### Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône **Administration** dans vos applications pour lancer la console :

![Icône Administration](img/entidf/admin-icon.png)

Une fois dans celle-ci, cliquez sur le lien « Gérer les connecteurs » de la rubrique « Services » :

![Services Administration](img/ent_edifice/admin_services.png)

### Créer un connecteur

Cliquez sur le bouton « Créer un connecteur »

![Creer un connecteur](img/ent_edifice/creer_connecteur.png)

### Paramétrer le connecteur

#### 1. Téléversez un fichier pour l'icône :


![Televerser l'icone](img/ent_edifice/upload_icone.png)

![Icone elea](img/ent_edifice/logo_elea.png) 

(ou renseignez l'url : `https://[identifiant_plateforme].elea.apps.education.fr/theme/vital/pix/logo.png`)

#### 2. Utilisez le tableau ci-dessous pour compléter les autres paramètres :

   ![Paramètres](img/ent_edifice/parametres_lien.png)

   | Paramètre       | Valeur                                                                                            |
      |-----------------|---------------------------------------------------------------------------------------------------|
   | Identifiant     | éléa[+code_territorial]                                                                           |
   | Nom d'affichage | ÉLÉA                                                                                              |
   | URL             | `https://[identifiant_plateforme].elea.apps.education.fr/login/index.php?multiCAS=[identifant_ENT]`|
   | Cible           | Nouvelle page                                                                                     |

   **ATTENTION** : L'URL doit être adaptée à votre situation en vous aidant du tableau [en annexe](#annexe---identifiant-ent).
   
Exemple d'URL valide : https://**pdl-44-nord**.elea.apps.education.fr/login/index.php?multicas=**eprimo**

:::warning
Si vous administrez l'ensemble de l'ENT, il est préférable de définir un seul connecteur par plateforme ÉLÉA et de l'attribuer aux utilisateurs concernés. Il faut dans ce cas cocher la case "partager le connecteur avec les sous-structures".

Si vous administrez uniquement votre établissement, **le champ identifiant doit être unique**, sous la forme elea+UAI en minuscule (ex. elea971000a).
:::

#### 3. Configurez le champ spécifique CAS

Cliquez sur le menu « Champs spécifiques CAS »

![Champs specifiques CAS](img/ent_edifice/champs_cas.png)

Cochez la case « Activer le champ spécifique CAS »
Sélectionnez « Défaut » dans le menu déroulant « Type »

#### 4. Validez la création

N'oubliez pas de cliquer sur le bouton « Créer »

![Creer](img/ent_edifice/bouton_creer.png)

### Attribuer les droits

Pour terminer, attribuez les droits d'accès à ce connecteur via l'onglet "attribution" :

![Attribution des droits](img/ent_edifice/attribution.png)

Vous devriez donner l'accès aux profils suivants :

- Tous les enseignants
- Tous les élèves
- Tous les personnels

*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices.*

### Annexe - Identifiant ENT

| Projet ENT             | Identifiant ENT pour multiCAS                     |
|------------------------|---------------------------------------------------|
| monlycée.net           | entidf                                            |
| ENT Hauts-de-France    | enthdf                                            |
| ENT Guadeloupe         | guadeloupe                                        |
| ENT77                  | ent77                                             |
| e-collège Yvelines     | dept78                                            |
| mon collège (Essonne)  | ent91                                             |
| e-primo                | eprimo                                            |
| e-lyco                 | elyco / pdl2d                                     |
| L’Educ de Normandie    | educnorm                                          |
| Paris Classe Numérique | pcn75                                             |
| Corse                  | corse                                             |
| Mayotte                | mayotte                                           |
| Wilapa                 | guyane                                            |
| 04                     | pacaclg04                                         |
| 05                     | pacaclg05                                         |
| 13                     | pacaclg13                                         |
| 83                     | pacaclg83                                         |
| Martinique             | martinique                                        |
| Polynésie              | polynesie                                         |
| Réunion 1D             | ent1dreu                                          |
