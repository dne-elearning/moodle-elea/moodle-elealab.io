### Créer les comptes utilisateurs avec l'ENT eCollège31

:::warning **Import facultatif**
Cet import est facultatif mais il est demandé de le faire au moins une fois après la création de l'établissement (cela permet l'initialisation complète de l'établissement).  
Par la suite, à la connexion d'un enseignant, les classes de son service sont créées (vides). A la connexion d'un élève, sa classe est créée si elle n'existe pas et il est ajouté à la classe.  
:::
#### Etape 1 - Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à votre ENT avec un compte administrateur ou de personnel de direction puis  - choisissez **Administration** (seuls les administrateurs voient cette icône).

Dans le menu de gauche, aller dans **Annuaire** qui se trouve dans **SERVICES ÉTABLISSEMENT** ou dans **SERVICES PERSONNELS**.


Un menu secondaire apparaît, cliquez sur **Administration** puis **Export CSV spécifique**.

![](img/entAURA/import1.png#printscreen)

À droite s'affiche à la zone de sélection

1. Laisser **profil** sur la valeur par défaut **Sélectionner un profil**.
2. Dans la colonne **Les champs disponibles**, cliquer sur **Nom**, **Prénom**, **Profil**, **Identifiant ENT**, **Classe**, ils vont basculer sur la colonne **votre sélection**

![](img/entAURA/import2.png#printscreen)


3. Cliquer sur **Valider**. Attendre que le fichier soit construit et téléchargé sur votre ordinateur (ce temps peut être d'une minute pour les gros établissements)



#### Étape 2 - Se connecter à la plateforme Éléa de l'établissement

Un courriel automatique est adressé au chef d'établissement / IEN via l'adresse de l'établissement ou de la circonscription.  
Il contient le lien permettant **d'activer le compte local de gestion de l'établissement** sur la plateforme Éléa, puis d'y **importer les comptes utilisateurs.**

Aller sur votre plateforme Éléa sans passer par le connecteur ENT.  
Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![accueil_elea](img/accueil_elea/capture2_95.png#printscreen)

Puis entrez **l'UAI (RNE) et le mot de passe.**

![accueil_elea](img/accueil_elea/capture3.png#printscreen)

#### Étape 3 - Importer vos comptes utilisateurs dans Éléa

Sur la page d'accueil, cliquez sur **Importer des utilisateurs**.

![import](img/entAURA/import3.png#printscreen)

Puis cliquez sur **« Choisir un fichier »** ou **glissez/déposez** celui-ci.

:::danger **Très important** :  
Le fichier doit impérativement  se nommer  **« export.csv »** et vous devrez le détruire à la fin de la procédure car il contient des données personnelles ! 

Si le fichier se nomme autrement (par exemple **« export (1).csv »**), c'est que vous avez sans doute omis de supprimer le précédent export réalisé sur votre machine. Supprimez alors le plus vieux et renommez le plus récent en supprimant les parenthèses et le chiffre à l'intérieur.
:::

![import](img/entAURA/import4.png#printscreen)

Patientez pendant la création des cohortes et des utilisateurs. La procédure est automatique et ne demande aucune action de votre part.

![creation_cohortes](img/import/capture7.png#printscreen)

![creation_cohortes](img/import/capture7bis.png#printscreen)

Attendez l’affichage du message **« importation terminée ».**

![importation_terminée](img/import/capture10.png#printscreen)
