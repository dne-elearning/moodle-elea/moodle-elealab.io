### Créer un connecteur Éléa sur Mon ENT Occitanie

#### Accéder à la liste des services

Connectez-vous à l'ENT avec les identifiants d'un compte ayant les pouvoirs d'administrateur pour votre établissement

Dans le menu de gauche, cliquez sur **ADMINISTRATION**.

![](img/entAURA/capture2.png#printscreen#centrer)

Un menu secondaire apparaît et indique que vous vous trouvez dans les sous-sections **Services > Liste des services**. La liste des services existants s'affiche à droite de l'écran.

![](img/entAURA/capture10.png#printscreen#centrer)

#### Créer un nouveau connecteur

Cliquez sur **Nouveau service** en haut à droite.

![](img/entAURA/capture11.png#printscreen#centrer)

Complétez les seuls champs **Type de SSO**, **URL** et **Intitulé** en vous aidant de la capture et du tableau suivants :

![](img/entAURA/capture12.png#printscreen#centrer)

| Paramètre   | Valeur                                                       |
| ----------- | ------------------------------------------------------------ |
| Type de SSO | Pas de SSO ou SSO standard                                   |
| URL         | voir ci-dessous |
| Intitulé    | Éléa                                                         |

:::info **Remarques**
 L'URL ci-dessus est uniquement valable pour les établissements du bassin de Pontoise. Vous devez l'adapter à votre situation en vous aidant du tableau en annexe.
Vous pouvez modifier **Regroupement** pour que le connecteur apparaisse aux utilisateurs dans une autre catégorie. Pourquoi pas dans **Pédagogie** ou **Ressources numériques** ?
:::

#### Partager votre connecteur

Cliquez sur l'onglet **Accès population**.

![](img/entAURA/capture13.png#printscreen#centrer)

1. Sélectionnez le rôle **Accès au service** dans la première liste déroulante ;
2. Sélectionnez le profil **Elève** dans la seconde ;
3. Cliquez sur **Ajouter les accès**.
4. Recommencez les étapes 2 et 3 avec les profils **Enseignant**, **Non enseignant** et **Autre**.

![](img/entAURA/capture14.png#printscreen#centrer)

> **Remarque** : les parents et/ou tuteurs n'accèdent pas à Éléa.

#### Annexe - URL des bassins concernés

| Nom                              | Adresse                                                                          |
| -------------------------------- | -------------------------------------------------------------------------------- |
| Aude                             | https://occ-11.elea.apps.education.fr/login/index.php?multicas=occitanie         |
| Gars                             | https://occ-30.elea.apps.education.fr/login/index.php?multicas=occitanie         |
| Pyrénées-Orientales et Andorre   | https://occ-66-andorre.elea.apps.education.fr/login/index.php?multicas=occitanie |
| Hérault                          | https://occ-34.elea.apps.education.fr/login/index.php?multicas=occitanie         |
| Hérault et Lozère                | https://occ-34-48.elea.apps.education.fr/login/index.php?multicas=occitanie      |
| Haute-Garonne sud                | https://occ-31-sud.elea.apps.education.fr/login/index.php?multicas=occitanie     |
| Haute-Garonne centre             | https://occ-31-centre.elea.apps.education.fr/login/index.php?multicas=occitanie  |
| Haute-Garonne nord               | https://occ-31-nord.elea.apps.education.fr/login/index.php?multicas=occitanie    |
| Ariège, Gers et Hautes-Pyrénées  | https://occ-09-32-65.elea.apps.education.fr/login/index.php?multicas=occitanie   |
| Lot et Tarn-et-Garonne           | https://occ-46-82.elea.apps.education.fr/login/index.php?multicas=occitanie      |
| Aveyron et Tarn                  | https://occ-12-81.elea.apps.education.fr/login/index.php?multicas=occitanie      |