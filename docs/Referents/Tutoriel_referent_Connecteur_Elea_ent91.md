---
tags:
  - connecteur
  - MonCollège
  - ENT
---

import Tabs from '@theme/Tabs';
import TabItem from '@theme/TabItem';

## Créer un connecteur Éléa sur MonCollège

:::tip Bon à savoir
Le délai entre la création du connecteur et son bon fonctionnement peut varier de quelques minutes à plusieurs heures. Un peu de patience avant de tester.
:::


### Accéder à la console

Connectez-vous à l'ENT avec un compte administrateur. Si vous l'êtes bien, vous trouverez l'icône **Administration** dans vos applications pour lancer la console.

![admin-icon](img/ent91/admin-icon.png#printscreen#centrer)

### Créer un connecteur

1. Une fois dans celle-ci, cliquez sur le lien « **Gérer les connecteurs** » de la rubrique « **Services** » :

![Services Administration](img/ent_edifice/admin_services.png#printscreen#centrer)

2. Puis en haut à droite sur **Créer un connecteur**.

![connecteur](img/ent91/connecteur.png#printscreen#centrer)

### Paramétrer votre connecteur

#### 1. Téléversez un fichier pour l'icône :


![Téléverser l'icône](img/ent_edifice/upload_icone.png#printscreen#centrer)

![Icône ÉLÉA](img/ent_edifice/logo_elea.png#printscreen#centrer) 

(ou renseignez l'url : `https://versailles.elea.ac-versailles.fr/theme/vital/pix/logo.png`)

#### 2. Utilisez le tableau ci-dessous pour compléter les autres paramètres :

  ![Paramètres](img/ent_edifice/parametres_lien.png#printscreen#centrer)

   | Paramètre       | Valeur                                                                   |
   |-----------------|--------------------------------------------------------------------------|
   | Identifiant     | éléa suivi de votre UAI/RNE (avec accents, minuscules obligatoires et sans espace)                |
   | Nom d'affichage | ÉLÉA                                                                     |
   | URL             | `https://[bassin].elea.ac-versailles.fr/login/index.php?multicas=ent91` |
   | Cible           | Nouvelle page                                                            |

  :::danger Attention
  L'URL doit être adaptée à votre situation en vous aidant du tableau ci-dessous.
  :::

**Adresse URL de chaque bassin**

| Nom       | Adresse                                                                |
| --------- | ---------------------------------------------------------------------- |
| Étampes   | https://etampes.elea.ac-versailles.fr/login/index.php?multicas=ent91   |
| Évry      | https://evry.elea.ac-versailles.fr/login/index.php?multicas=ent91      |
| Massy     | https://massy.elea.ac-versailles.fr/login/index.php?multicas=ent91     |
| Montgeron | https://montgeron.elea.ac-versailles.fr/login/index.php?multicas=ent91 |
| Savigny   | https://savigny.elea.ac-versailles.fr/login/index.php?multicas=ent91   |


#### 3. Configurez le champ spécifique CAS

Cliquez sur le menu « Champs spécifiques CAS »

![Champs spécifiques CAS](img/ent_edifice/champs_cas.png#printscreen#centrer)

Cochez la case « Activer le champ spécifique CAS »
Sélectionnez « Défaut » dans le menu déroulant « Type »

#### 4. Validez la création

N'oubliez pas de cliquer sur le bouton « Créer »

![Créer](img/ent_edifice/bouton_creer.png#printscreen#centrer)

#### 5. Attribuer les droits

Pour terminer, attribuez les droits d'accès à ce connecteur via l'onglet "attribution" :

![Attribution des droits](img/ent_edifice/attribution.png#printscreen#centrer)

Vous devriez donner l'accès aux profils suivants :

- Tous les enseignants
- Tous les élèves
- Tous les personnels

:::info
*Les parents n'ont pas accès à Éléa puisqu'ils n'ont pas vocation à créer des parcours et qu'il n'est pas attendu qu'ils réalisent les exercices.*
:::
