
Ce compte de référent n'est pas lié à votre compte enseignant. Par ailleurs, il ne vous permettra que de gérer la plateforme ÉLÉA de l'établissement, en aucun cas à créer des cours.

Pour initialiser votre compte de gestion, cliquez sur le lien communiqué dans le mail reçu :

![Exemple mail](img/PACA/manager_1.png#centrer#printscreen)

:::danger
**N.B.** : identifiant (non modificable) est le code **UAI/RNE** de l'établissement.
:::

Saisissez deux fois de suite le mot de passe choisi ❶ - ❷ (en respectant scrupuleusement les critères indiqués) puis cliquez sur **ENREGISTRER** :

![mot de passe](img/PACA/manager_2.png#centrer#printscreen)

Vous pourrez à partir de cette interface :
4. Importer des utilisateurs
5. Gérer les cohortes
6. Gérer les enseignants

![managerboard](img/PACA/manager_3.png#centrer#printscreen)
