#### Créer un connecteur sur e-lyco

Le connecteur est géré directement par les équipes itslearning. Il est déployé automatiquement sur tous les établissements publics et privés de l'académie et est disponible pour les personnels et élèves.

![Connecteur-elyco.png](img%2Fent_elyco%2FConnecteur-elyco.png)

Si ce n'est pas le cas pour votre établissement, merci de déposer une demande d'assistance sur : 
https://assistance.ac-nantes.fr