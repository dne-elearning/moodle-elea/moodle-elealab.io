### Importer les comptes utilisateurs avec l'ENT Néo
:::info
**Si vous le souhaitez, il est possible d'importer les élèves, les classes et les enseignants en début d'année.**

**++Cette opération est optionnelle++ :
Elle permet de disposer des classes peuplées des élèves avant que ceux ci se connectent. Mais pas des groupes d'enseignement.**
:::
#### Etape 1 : Exporter vos comptes utilisateurs depuis l'ENT

Connectez-vous à votre ENT (https://moncollege-ent.var.fr) avec votre compte, puis dans la liste des applications, choisissez **Administration** 

:::warning
**Rappel :** La console d'administration n'est accessible qu'aux membres du groupe fonction/discipline ***Administrateurs locaux de ...***.
:::

Cliquez sur **Exporter des comptes** dans la section **Imports / Exports**.

![](img/PACA/edifice_import_1.png#printscreen#centrer)

Vérifier que le configuration de la configuration soit :
- Type d'export : **Export classique**
- Classe : **Toutes**
- Profil : **Tous**
- Filtre : **Pas de filtre sur l'activation**

![](img/PACA/edifice_import_2.png#printscreen#centrer)


Cliquer sur ![](img/PACA/edifice_import_3.png#printscreen#centrer)

Sauvegardez le fichier "**export.csv**" et notez où a été sauvegardé ce fichier  sur votre ordinateur.

#### Etape 2 : Se connecter à la plateforme Elea de l'établissement avec le compte de gestion

Au moment de l'activation de la catégorie de votre établissement sur l'instance ÉLÉA dont vous dépendez, un courriel automatique a été adressé au chef d'établissement / IEN via l'adresse `UAI@ac-nice.fr`.
Il contenait le lien permettant d'activer le **compte de gestion de l'établissement** sur la plateforme Éléa (voir le tuoriel ( pour l'activation du compte de référent de votre établissement).

Accédez à l'URL correspondante à votre plateforme. 

![](img/PACA/edifice_import_4.png#printscreen#centrer)


Cliquez sur **Choisissez votre compte**, sélectionnez **Autres utilisateurs** et cliquez sur ![](img/PACA/edifice_import_5.png#printscreen#centrer).

![](img/PACA/edifice_import_6.png#printscreen#centrer)

Saisissez le **code UAI/RNE** qui correspond à votre identifiant de compte de gestion et le **mot de passe associé** que vous avez choisi à son activation.

#### Etape 3 : Importer vos comptes utilisateurs dans Elea

Sur la page de gestion de votre établissement, l'onglet "Accueil" vous donne l'historique de vos derniers imports.

![](img/PACA/edifice_import_7.png#printscreen#centrer)

Accédez à l'onglet « **Importer des utilisateurs** ».

Cliquez sur « **Sélectionnez un fichier sur votre ordinateur** » ou **glissez/déposez** le fichier "**export.csv**".

![](img/PACA/edifice_import_8.png#printscreen#centrer)

![](img/PACA/edifice_import_9.png#printscreen#centrer)

Attendez l’affichage du message « **Importation terminée** ».

![](img/PACA/edifice_import_10.png#printscreen#centrer)

:::warning
**N.B. :** Via cet import, **seules les ++cohortes correspondantes aux classes++ seront créées et peuplées**. Les ++cohortes de groupes d'enseignement++ se créeront à la volée lorsque les utilisateurs professeurs se connecteront à la plateforme ÉLÉA, et se peupleront (voire se créeront, si elles n'existent pas encore) lorsque les utilisateurs élèves se connecteront à leur tour. Il faudra bien sur que ces structures soient créées et peuplées par des élèves au niveau de SIECLE par la direction de l'établissement.
:::