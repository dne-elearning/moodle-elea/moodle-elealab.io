#### Créer les comptes utilisateurs avec e-lyco

Les comptes élèves et enseignants sont créés automatiquement à la première connexion des utilisateurs. Cela rend toute opération manuelle inutile. La fonctionnalité d'importation n'est donc pas opérante pour les établissements du projet e-lyco.

#### Gestion des cohortes
De la même manière, à chaque connexion d'un utilisateur les classes auxquelles il est rattaché sont créées ou mises à jour. Les changements de cohortes sont donc répercutés automatiquement. Toutefois si une cohorte n'est plus utilisée et qu'elle ne contient plus de participants, il est possible de la supprimer manuellement.