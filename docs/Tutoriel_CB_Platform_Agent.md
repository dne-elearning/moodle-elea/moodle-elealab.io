### Paramétrer la plateforme Éléa

Ce tutoriel est destiné aux gestionnaires plateformes d'Éléa (conseiller de bassin dans l'académie de Versailles).

Il décrit :
- comment personnaliser la plateforme
- gérer la transition d'année
- créer un établissement

En tant que **responsable d'une zone géographique ou d'un bassin d'éducation** , vous disposez d'un compte local spécifique sur la plateforme dont le nom d'utilisateur est « **manager** ». Sur certaines zones géographiques, il peut y avoir plusieurs managers : le nom d'utilisateur peut alors être « **manager1** » ou « **manager2** »...
Pour créer votre mot de passe, utilisez la procédure d'oubli de mot de passe en précisant ce nom d'utilisateur (« manager ») pour recevoir un lien par courriel.
Chaque compte est associé à une adresse mail par l'administrateur de la plateforme. En cas de changement, lui seul pourra effectuer la modification.

#### Vous connecter à la plateforme en tant que gestionnaire

Entrez l'url correspondant à votre plateforme de bassin ou zone géographique.

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![page_accueil_elea](images_manager/capture1.png)

Puis entrez l'identifiant **« manager »** et le mot de passe.

![connexion_manager](images_manager/capture3.png)

#### Personnalisation de la plateforme

Il est possible de personnaliser le nom de la région académique et le nom de la zone géographique pour chaque plateforme.

- Cliquez sur l'onglet "Paramétrage", puis complétez les deux premiers champs.

![paramétrer_plateforme](images_manager/capture1b.png)

- cliquez sur Enregistrer au bas de la page.

Vous pouvez également paramétrer les liens des boutons "Échanger entre pairs" et "Écrire au support".

- Complétez les champs dédiées dans l'onglet "Paramétrage"

![paramétrer_plateforme](images_manager/capture2b.png)

- cliquez sur Enregistrer au bas de la page.

### Gérer la transition d'année

Une procédure de transition d’année a lieu tous les ans durant l'été pour supprimer toutes les cohortes (classes) sur l’ensemble des parcours.

En debut d'année, le gestionnaire plateforme doit autoriser le démarrage de l’année scolaire dans Éléa pour que les classes puissent être créées et éviter ainsi que des élèves ne soient affectés aux anciennes classes.

Dès que les ENT ont fait leur bascule d'année et que les structures dans les établissements sont stabilisées, vous pourrez indiquer dans la plateforme que l'année scolaire a démarré.

- Après authentification, cliquez sur l'onglet "Paramétrage", comme ci-dessus, puis en bas de la page, basculer le sélecteur "Année scolaire démarrée" sur "Oui" (celui-ci est remis à non par le script de transition d'année).

![transition_d_annee](images_manager/capture3b.png)

- cliquez sur Enregistrer au bas de la page.

### Créer un établissement sur Éléa

La suite de ce tutoriel décrit la procédure permettant de créer un établissement sur les plateformes Éléa.

#### Étape 1

Entrez l'url correspondant à votre plateforme de bassin ou zone géographique.

Cliquez sur « **Choisissez votre compte** » puis sur « **Utiliser mon compte local** ».

![page_accueil_elea](images_manager/capture1.png)

Puis entrez l'identifiant **« manager »** et le mot de passe.

![connexion_manager](images_manager/capture3.png)

#### Étape 2

Les établissements déjà existants sur la plateforme sont listés par commune.

![liste_etablissements](images_manager/capture4b.png)

Cliquez sur **« Ajouter un nouvel établissement »**.

![ajouter_etablissement](images_manager/capture5b.png)

Complétez **l'UAI de l'établissement** que vous souhaitez ajouter, puis cliquez sur **« Rechercher »**.

![UAI_etablissement](images_manager/capture6.png)

Les informations relatives à l'établissement sont pré-saisies.
Cliquez sur **« Valider »** si elles sont correctes. Les modifications restent exceptionnelles.

![validation_infos](images_manager/capture7.png)

Un message vous informe que l'établissement a été ajouté avec succès. 
Vous pouvez continuer à ajouter des établissements ou fermer la fenêtre si vous souhaitez terminer la procédure.

![fin_ajout](images_manager/capture8.png)

La liste des établissements est mise à jour automatiquement lorsque vous quittez l'assistant.

![liste_etablissements](images_manager/capture9.png)

:::warning IMPORTANT ! 

 **Il faut vérifier le contenu de la colonne CAS. S'il n'est pas fait mention d'un ENT alors que l'établissement en possède un ou bien que la solution ENT mentionnée n'est pas la bonne, écrire au support.
Seuls les administrateurs pourront procéder à la correction.**
:::

#### Étape 3

A l'issue de la création d'un établissement, un courriel automatique est envoyé au chef d'établissement ou à l'IEN de circonscription, l'informant qu'il peut désormais administrer son espace sur Eléa.
Il pourra à ce moment-là **créer les comptes utilisateurs** ou déléguer cette opération au référent Eléa.

:::info
Si le chef d'établissement a perdu les identifiants de ce compte local, vous pouvez lui communiquer la documentation correspondante, le tutoriel **« [Tuto référent réinitialiser sont mot de passe](https://dne-elearning.gitlab.io/moodle-elea/documentation/docs/Referents/?ENTidf=R%C3%A9initialiser+son+mot+de+passe#ent-lyc%C3%A9es) »**. Le nom d'utilisateur correspond à l'UAI de l'établissement.

Si malgré cela des difficultés persistent, vous pouvez adresser un courriel au support Eléa **support-elea@ac-versailles.fr** en l'accompagnant impérativement d'une ou plusieurs captures d'écran faisant apparaître le problème.
:::

