// @ts-check
// `@type` JSDoc annotations allow editor autocompletion and type checking
// (when paired with `@ts-check`).
// There are various equivalent ways to declare your Docusaurus config.
// See: https://docusaurus.io/docs/api/docusaurus-config

import {themes as prismThemes} from 'prism-react-renderer';

/** @type {import('@docusaurus/types').Config} */
const config = {
  title: 'Documentation pour l\'utilisation d\'Éléa',
  tagline: 'Commun numérique proposé par la DNE',
  favicon: 'img/elea.ico',

  // Set the production url of your site here
  url: 'https://dne-elearning.gitlab.io/',
  // Set the /<baseUrl>/ pathname under which your site is served
  // For GitHub pages deployment, it is often '/<projectName>/'
  baseUrl: '/moodle-elea/documentation/',

  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',

  // Even if you don't use internationalization, you can use this field to set
  // useful metadata like html lang. For example, if your site is Chinese, you
  // may want to replace "en" with "zh-Hans".
  i18n: {
    defaultLocale: 'fr',
    locales: ['fr'],
  },

  presets: [
    [
      'classic',
      /** @type {import('@docusaurus/preset-classic').Options} */
      ({
        docs: {
          sidebarPath: './sidebars.js',
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
         },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          // Remove this to remove the "edit this page" links.
        },
        theme: {
          customCss: './src/css/custom.css',
        },
      }),
    ],
  ],

  themeConfig:
    /** @type {import('@docusaurus/preset-classic').ThemeConfig} */
    ({
      // Replace with your project's social card
      image: 'img/elea.png',
      navbar: {
        title: 'Documentation',
        logo: {
          alt: 'logo Éléa',
          src: 'img/elea.png',
        },
        items: [
          {
            type: 'doc',
            docId: 'Professeurs',
            position: 'left',
            label: 'Professeurs',
          },
          {
            type: 'doc',
            docId: 'Referents',
            position: 'left',
            label: 'Référents',
          },
          {
            type: 'doc',
            docId: 'Formateurs',
            position: 'left',
            label: 'Formateurs',
          },
          {
            type: 'doc',
            docId: 'Conseillers',
            position: 'left',
            label: 'Conseillers',
          },
          {
            type: 'doc',
            docId: 'FAQ',
            label: '❓FAQ',
            position: 'right',
          },
        ],

      },
      algolia: {
        // L'ID de l'application fourni par Algolia
        appId: 'D8X21ZDUOW',
  
        // Clé d'API publique : il est possible de la committer en toute sécurité
        apiKey: 'bcddf88fcc7d6c7fb89990dc4d1e3fd2',
  
        indexName: 'dne-elearning-gitlab',
  
        // Facultatif : voir la section doc ci-dessous
        contextualSearch: true,

        // Facultatif : paramètres de recherche de Algolia
        searchParameters: {},
  
        // Facultatif : chemin pour la page de recherche qui est activée par défaut (`false` pour le désactiver)
        searchPagePath: 'search',
  
        //... autres paramètres de Algolia
      },
      footer: {
        style: 'dark',
        links: [{
           title: 'Articles',
           items: [
            {
              label:'Nouveautés',
              to: '/blog',
            },
            {
              label: 'Présentation de la plateforme Éléa (Versailles)',
              href: 'https://ressources.dane.ac-versailles.fr/ressource/elea',
            },
           ],
          },
           {
            title: 'Communautés nationales',
            items: [
              {
                label: 'Inter-académique Moodle',
                href: 'https://iam.unistra.fr',
              },
              
            ],
          },
          {
            title: 'Communautés locales',
            items: [
              {
                label: 'Échanger entre pairs (Versailles)',
                href: 'https://magistere.education.fr/ac-versailles/course/view.php?id=16570',
              },
              {
                label: 'Échanger entre pairs (Bretagne)',
                href: 'https://magistere.education.fr/ac-rennes/course/view.php?id=8377',
              },
            ],
          },
        ],
        copyright: `Équipe Éléa nationale, Ministère de l'Éducation nationale et de la Jeunesse `,
      },
      prism: {
        theme: prismThemes.github,
        darkTheme: prismThemes.dracula,
      },
    }),
};

export default config;
